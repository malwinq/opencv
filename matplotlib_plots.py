import cv2
from matplotlib import pyplot as plt

thresh_type = cv2.THRESH_BINARY
ad_thr_type = cv2.ADAPTIVE_THRESH_MEAN_C
max_value = 255
block_size = 5
c = 11

img = cv2.imread('lenna.jpg', -1)
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
th = cv2.adaptiveThreshold(img, max_value, ad_thr_type,
                           thresh_type, block_size, c)
titles = ['original', 'thresholind']
images = [img, th]

for i in range(len(images)):
    plt.subplot(1, len(images), i+1)
    plt.imshow(images[i], 'gray')
    plt.title(titles[i])
    plt.xticks([]), plt.yticks([])

plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()
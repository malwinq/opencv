import cv2

events = [i for i in dir(cv2) if 'EVENT' in i]
color1 = (200, 200, 1)
color2 = (150, 150, 50)
thickness = 2
font = cv2.FONT_HERSHEY_COMPLEX

def click_event(event, x, y, flags, param):
    strXY = str(x) + ', ' + str(y)
    blue = img[y, x, 0]
    green = img[y, x, 1]
    red = img[y, x, 2]
    strRGB = str(blue) + ', ' + str(green) + ', ' + str(red)
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.putText(img, strXY, (x, y), font, 1, color1, thickness)
    if event == cv2.EVENT_RBUTTONDOWN:
        cv2.putText(img, strRGB, (x, y), font, 1, color2, thickness)
    cv2.imshow('image', img)

img = cv2.imread('lenna.jpg', 1)
cv2.imshow('image', img)

cv2.setMouseCallback('image', click_event)

cv2.waitKey(0)
cv2.destroyAllWindows()
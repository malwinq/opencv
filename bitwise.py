import cv2
import numpy as np

img1 = np.zeros((512, 512, 3), np.uint8)
img1 = cv2.rectangle(img1, (200, 0), (300, 100), (255, 255, 255), -1)
img2 = cv2.imread('lenna.jpg')

img = cv2.bitwise_and(img1, img2)
# img = cv2.bitwise_or(img1, img2)
# img = cv2.bitwise_xor(img1, img2)


cv2.imshow('image', img)

cv2.waitKey(0)
cv2.destroyAllWindows()
import cv2
import numpy as np

events = [i for i in dir(cv2) if 'EVENT' in i]
color1 = (200, 200, 1)
color2 = (150, 150, 50)
thickness = -1
radius = 3
font = cv2.FONT_HERSHEY_COMPLEX

def click_event(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img, (x, y), 3, color1, thickness)
        points.append((x, y))
        if len(points) >= 2:
            cv2.line(img, points[-2], points[-1], color2, 5)
    cv2.imshow('image', img)

img = np.zeros((512, 512, 3), np.uint8)
cv2.imshow('image', img)
points = []

cv2.setMouseCallback('image', click_event)

cv2.waitKey(0)
cv2.destroyAllWindows()
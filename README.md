# OpenCV

OpenCV with Python projects.

## Prerequisites

Using opencv-python 4.1.1.26 with Python 3.7

```
pip install opencv-python
```
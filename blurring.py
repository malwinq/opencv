import cv2
import numpy as np
import matplotlib.pyplot as plt

thres_min = 120
thres_max = 255
size = (5, 5)
iteration = 2
depth = -1

img = cv2.imread('lenna.jpg')
img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

kernel = np.ones(size, np.float32) / (size[0] * size[1])
dst = cv2.filter2D(img, depth, kernel)
blur = cv2.blur(img, size)
gaussian = cv2.GaussianBlur(img, size, 0)
# median is for salt-and-pepper noise
median = cv2.medianBlur(img, size[0])
# bilateral filter for sharpening edges
bilateral = cv2.bilateralFilter(img, 9, 75, 75)

titles = ['image', 'filer', 'blur',
          'gaussian', 'median', 'bilateral']
images = [img, dst, blur,
          gaussian, median, bilateral]

for i in range(len(images)):
    plt.subplot(2, len(images) // 2 + len(images) % 2, i+1)
    plt.imshow(images[i], 'gray')
    plt.title(titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()

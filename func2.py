import cv2

img = cv2.imread('lenna.jpg')
# weights
w1 = 1
w2 = 5


print(img.shape)    # tuple (rows, columns, channels)
print(img.size)     # number of pixels
print(img.dtype)    # data type
b, g, r = cv2.split(img)
img = cv2.merge((b, g, r))

eye = img[261:275, 246:291]
img[222:236, 301:346] = eye
eye = cv2.resize(img, (512, 512))
# img2 = cv2.add(img, eye)
img2 = cv2.addWeighted(img, w1, eye, w2, 0)

cv2.imshow('image', img2)
cv2.waitKey(0)
cv2.destroyAllWindows()

import cv2 as cv

threshold = 127
thresh_type1 = cv.THRESH_BINARY
thresh_type2 = cv.THRESH_TOZERO
img = cv.imread('ciastka.jpg', 0)

cv.imshow('image', img)

ret, th1 = cv.threshold(img, threshold, 255, thresh_type1)
cv.imshow('th1', th1)

ret, th2 = cv.threshold(img, threshold, 255, thresh_type2)
cv.imshow('th2', th2)

cv.waitKey(0)
cv.destroyAllWindows()
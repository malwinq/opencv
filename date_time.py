import cv2
from datetime import datetime

cap = cv2.VideoCapture(0)
font = cv2.FONT_HERSHEY_SIMPLEX
p1 = (20, 30)
p2 = (100, 80)
thickness = 2
color = (100, 200, 50)

while cap.isOpened():
    ret, frame = cap.read()

    if ret == True:
        text = 'Width: ' + str(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
        text = text + ' Height: ' + str(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
        datet = str(datetime.now())
        frame = cv2.putText(frame, text, p1, font, 1, color, thickness)
        frame = cv2.putText(frame, datet, p2, font, 1, color, thickness)
        cv2.imshow('frame', frame)

        if cv2.waitKey(1) == ord('q'):
            break
    else:
        break

cap.release()
cv2.destroyAllWindows()

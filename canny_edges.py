import cv2
import numpy as np
import matplotlib.pyplot as plt

""" Canny edge detection: noise reduction (gaussian) -> gradient calc ->
    non-maximum suppression -> double threshold -> edge tracking by hysteresis """

thres1 = 150
thres2 = 200

img = cv2.imread('lenna.jpg', 0)
canny = cv2.Canny(img, thres1, thres2, )

titles = ['image', 'canny']
images = [img, canny]

for i in range(len(images)):
    plt.subplot(2, len(images) // 2 + len(images) % 2, i+1)
    plt.imshow(images[i], 'gray')
    plt.title(titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()

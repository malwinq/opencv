import cv2
import numpy as np

# img = cv2.imread('lenna.jpg', 1)

img = np.zeros([512, 512, 3], np.uint8)

p1 = (5, 5)
p2 = (405, 405)
p3 = (100, 500)
color = (0, 255, 0)
arr_color = (100, 200, 0)
thickness = 5
radius = 90

img = cv2.line(img, p1, p2, color, thickness)
img = cv2.arrowedLine(img, p3, p2, arr_color, thickness)
img = cv2.rectangle(img, p2, p3, color, thickness)
img = cv2.circle(img, p2, radius, arr_color, thickness, -1)

font = cv2.FONT_ITALIC
img = cv2.putText(img, 'Hello', p3, font, 3, arr_color, thickness, )

cv2.imshow('shapes', img)

cv2.waitKey(0)
cv2.destroyAllWindows()

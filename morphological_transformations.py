import cv2
import numpy as np
import matplotlib.pyplot as plt

thres_min = 120
thres_max = 255
kernel = np.ones((2, 2), np.uint8)
iteration = 2

img = cv2.imread('lenna.jpg', cv2.IMREAD_GRAYSCALE)
_, mask = cv2.threshold(img, thres_min, thres_max, cv2.THRESH_BINARY_INV)

dilation = cv2.dilate(mask, kernel, iterations=iteration)
erosion = cv2.erode(mask, kernel, iterations=iteration)
# opening: erosion -> dilation
opening = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
# closing: dilation -> erosion
closing = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
mg = cv2.morphologyEx(mask, cv2.MORPH_GRADIENT, kernel)
th = cv2.morphologyEx(mask, cv2.MORPH_TOPHAT, kernel)

titles = ['image', 'mask', 'dilation', 'erosion',
          'opening', 'closing', 'mg', 'th']
images = [img, mask, dilation, erosion,
          opening, closing, mg, th]

for i in range(len(images)):
    plt.subplot(2, len(images) // 2, i+1)
    plt.imshow(images[i], 'gray')
    plt.title(titles[i])
    plt.xticks([])
    plt.yticks([])

plt.show()

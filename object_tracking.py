import cv2
import numpy as np
from math import sqrt

""" Detecting colors from video capture"""

# params
color = (255, 255, 255)                     # text color
p0 = (50, 50)                               # text point
rect = np.ones([3, 3], np.uint8) * 255      # masking rectangle no1
rect2 = np.ones([10, 10], np.uint8) * 255   # masking rectangle no2

# callback function
def nothing(x):
    pass

cap = cv2.VideoCapture(0)

# params of trackbars - L lower limit, U upper limit
cv2.namedWindow('tracking')
cv2.createTrackbar('LH', 'tracking', 0, 255, nothing)
cv2.createTrackbar('LS', 'tracking', 0, 255, nothing)
cv2.createTrackbar('LV', 'tracking', 0, 255, nothing)

cv2.createTrackbar('UH', 'tracking', 255, 255, nothing)
cv2.createTrackbar('US', 'tracking', 255, 255, nothing)
cv2.createTrackbar('UV', 'tracking', 255, 255, nothing)

# loop
while cap.isOpened():

    # read frames from video
    ret, frame = cap.read()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    # limits of mask
    l_h = cv2.getTrackbarPos('LH', 'tracking')
    l_s = cv2.getTrackbarPos('LS', 'tracking')
    l_v = cv2.getTrackbarPos('LV', 'tracking')

    u_h = cv2.getTrackbarPos('UH', 'tracking')
    u_s = cv2.getTrackbarPos('US', 'tracking')
    u_v = cv2.getTrackbarPos('UV', 'tracking')

    lower = np.array([l_h, l_s, l_v])
    upper = np.array([u_h, u_s, u_v])

    # masking
    mask = cv2.inRange(hsv, lower, upper)
    mask = cv2.erode(mask, rect)
    mask = cv2.erode(mask, rect)
    mask = cv2.dilate(mask, rect2)
    mask = cv2.dilate(mask, rect2)

    # result
    res = cv2.bitwise_and(frame, frame, mask=mask)

    # counting the center point of object
    moment = cv2.moments(mask)

    if moment['m00'] == 0:
        text = 'Not found'

    else:
        x_m = moment['m10'] / moment['m00']
        y_m = moment['m01'] / moment['m00']
        text = 'x: ' + str(int(x_m)) + ', y: ' + str(int(y_m))
        size = int(sqrt(moment['m00']) / 40)
        p1 = (int(x_m) - size, int(y_m) - size)
        p2 = (int(x_m) + size, int(y_m) + size)
        frame = cv2.rectangle(frame, p1, p2, color, 2)

    # display
    frame = cv2.putText(frame, text, p0, cv2.FONT_HERSHEY_COMPLEX, 1, color, 2)
    cv2.imshow('image', frame)
    cv2.imshow('mask', mask)
    # cv2.imshow('res', res)

    key = cv2.waitKey(1)
    if key == 27:
        break

cap.release()
cv2.destroyAllWindows()
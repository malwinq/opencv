import cv2 as cv

# parameters
threshold = 127
thresh_type = cv.THRESH_BINARY
ad_thr_type = cv.ADAPTIVE_THRESH_MEAN_C
ad_thr_type2 = cv.ADAPTIVE_THRESH_GAUSSIAN_C
max_value = 255
block_size = 5
c = 11

img = cv.imread('lenna.jpg', 0)
cv.imshow('image', img)

ret, th1 = cv.threshold(img, threshold, 255, thresh_type)
cv.imshow('thresholding', th1)

th2 = cv.adaptiveThreshold(th1, max_value, ad_thr_type,
                           thresh_type, block_size, c)
cv.imshow('adaptive thresholding', th2)

th3 = cv.adaptiveThreshold(th1, max_value, ad_thr_type2,
                           thresh_type, block_size, c)
cv.imshow('adaptive thresholding gaussian', th3)

cv.waitKey(0)
cv.destroyAllWindows()